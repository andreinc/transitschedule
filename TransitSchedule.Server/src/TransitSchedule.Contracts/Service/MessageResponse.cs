﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Service
{
    public class MessageResponse<T> where T : new()
    {
        public T response;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageResponse{T}"/> class.
        /// </summary>
        public MessageResponse() : this(new T())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageResponse{T}"/> class.
        /// </summary>
        /// <param name="response">The response.</param>
        public MessageResponse(T response)
        {
            this.response = response;
            this.criteriaDateTime = DateTime.Now; 

        }
        /// <summary>
        /// Gets or sets the criteria date time.
        /// </summary>
        /// <value>
        /// The criteria date time.
        /// </value>
        public DateTime criteriaDateTime { get; set; }
    }
}

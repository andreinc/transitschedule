﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;

namespace TransitSchedule.CommonUtils.Assets
{
    public static class RouteUtils
    {

        private static readonly int defaultTakeLimit = 1;
        public static List<DateTime> NextOnSchedule(this RouteVO route, int stationId,  int count) 
        {
            return NextOnSchedule( route, stationId, DateTime.Now,  count);
        }

        public static List<DateTime> NextOnSchedule(this RouteVO route, int stationId)
        {
            return NextOnSchedule(route, stationId, DateTime.Now, defaultTakeLimit);
        }

        public static List<DateTime> NextOnSchedule(this RouteVO route, int stationId, DateTime when)
        { 
            return NextOnSchedule(route, stationId, when, defaultTakeLimit);
        }

        public static List<DateTime> NextOnSchedule(this RouteVO route, int stationId, DateTime when, int count)
        {
            var entries = route.Path.Where(r => r.Id == stationId).Single().GetScheduledStops(route).Where(ss => ss.ScheduledTime >= when).Take(count).Select(ss => ss.ScheduledTime);

            return entries.ToList();
        }
    }
}

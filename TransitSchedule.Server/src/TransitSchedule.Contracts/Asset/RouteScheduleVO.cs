﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Asset
{
    /// <summary>
    /// Simplified Object to return to the client serialized
    /// </summary>
    public class RouteScheduleVO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RouteScheduleVO"/> class.
        /// </summary>
        /// <param name="route">The route.</param>
        public RouteScheduleVO(RouteVO route )
        {
            this.RouteId = route.Id;
            this.RouteLabel = route.Label;
             
            this.NextArrivalSet = new List<DateTime>();
        }

        /// <summary>
        /// Gets or sets the route identifier.
        /// </summary>
        /// <value>
        /// The route identifier.
        /// </value>
        public int RouteId { get; set; }
        /// <summary>
        /// Gets or sets the route label.
        /// </summary>
        /// <value>
        /// The route label.
        /// </value>
        public string RouteLabel { get; set; }
        /// <summary>
        /// Gets or sets the next arrival set.
        /// </summary>
        /// <value>
        /// The next arrival set.
        /// </value>
        public List<DateTime> NextArrivalSet { get; set; }
    }
}

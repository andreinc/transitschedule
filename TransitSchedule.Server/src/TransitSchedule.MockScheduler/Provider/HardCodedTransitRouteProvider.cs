﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using TransitSchedule.Contracts.Service;

namespace TransitSchedule.MockScheduler.Provider
{
    public class HardCodedTransitRouteProvider : ITransitRouteProvider
    {
        /// <summary>
        /// Gets the network route set.
        /// </summary>
        /// <returns></returns>
        public IList<RouteVO> GetNetworkRouteSet()
        {
            var entries = new List<RouteVO>();

            //Hardcoded routes from problem definition
            entries.Add(new RouteVO(1, TimeSpan.Zero));
            entries.Add(new RouteVO(2, new TimeSpan(0, 2, 0)));
            entries.Add(new RouteVO(3, new TimeSpan(0, 4, 0)));

            return entries;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using TransitSchedule.Contracts.Business;
using TransitSchedule.Contracts.Service;
using TransitSchedule.MockScheduler.Provider;
using TransitSchedule.MockScheduler.Service;

namespace TransitSchedule.Business.Asset
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class TransitAuthorityAgent : ITransitController, IDisposable
    {

        #region Private
        /// <summary>
        /// The schedule builder
        /// </summary>
        private IScheduleBuilderService ScheduleBuilder;
        private ITransitNetworkProvider NetworkProvider;
        private ITransitRouteProvider RouteProvider;

        private const int defaultHoursForBuilder = 24;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TransitAuthorityAgent"/> class.
        /// </summary>
        public TransitAuthorityAgent() : this(
            new SimplifiedNonGeoScheduleBuilderService(), 
            new HardCodedTransitNetworkProvider(), 
            new HardCodedTransitRouteProvider() 
            )
        {

        }
         
        public TransitAuthorityAgent(
            IScheduleBuilderService scheduleBuilder,
            ITransitNetworkProvider networkProvider,
            ITransitRouteProvider routeProvider
            )
        {
            //TODO: IoC - framework/container

            this.ScheduleBuilder = scheduleBuilder;
            this.NetworkProvider = networkProvider;
            this.RouteProvider = routeProvider;
        }

        #endregion

        #region Life Cycle

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.Initialize(defaultHoursForBuilder, 0);
        }


        /// <summary>
        /// Initializes the business object
        /// </summary>
        /// <param name="hours">The hours.</param>
        /// <param name="minutes">The minutes.</param>
        public void Initialize(int hours, int minutes)
        {
            //Municipality serviced stations
            InitializeTransitNetwork();

            //Available Routes
            InitializeTransitRoutes();

            //Route Centric Init
            foreach (var route in AvailableRoutes)
            {
                //set station set to route
                route.Path = this.AvailableStations.ToList();

                //build/get the schedule from [strategy]
                this.ScheduleBuilder.PopulateSchedule(route, new TimeSpan(hours, minutes, 0));
            }
        }
        #endregion
         
        #region Properties


        /// <summary>
        /// Gets or sets the available routes.
        /// </summary>
        /// <value>
        /// The available routes.
        /// </value>
        public IList<RouteVO> AvailableRoutes { get; set; }

        /// <summary>
        /// Gets or sets the available stops.
        /// </summary>
        /// <value>
        /// The available stops.
        /// </value>
        public IList<StationVO> AvailableStations { get; set; }


        #endregion

        #region Members


        /// <summary>
        /// Initializes the stops.
        /// </summary>
        private void InitializeTransitNetwork()
        {
            //Hardcoded stops from problem definition
            this.AvailableStations = this.NetworkProvider.GetNetworkStationSet();
        }

        /// <summary>
        /// Initializes the routes.
        /// </summary>
        private void InitializeTransitRoutes()
        {
            this.AvailableRoutes = this.RouteProvider.GetNetworkRouteSet();
        }


        /// <summary>
        /// Gets the route.
        /// </summary>
        /// <param name="routeId">The route identifier.</param>
        /// <returns></returns>
        public RouteVO GetRoute(int routeId)
        {
            return AvailableRoutes.Where(r => r.Id == routeId).SingleOrDefault();
        }

        public void Dispose()
        {
            //explicit release of instances as needed 
        }


        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Service
{
    public interface IScheduleProviderService
    {
        /// <summary>
        /// Retrieves the schedule.
        /// </summary>
        /// <param name="stopId">The stop identifier.</param>
        /// <param name="routeId">The route identifier.</param>
        /// <param name="inquiryStartTime">The inquiry start time.</param>
        /// <param name="maxTimeSpanInMinutes">The maximum time span in minutes.</param>
        /// <returns></returns>
        List<DateTime> RetrieveSchedule(int stopId, int routeId, DateTime inquiryStartTime, TimeSpan maxTimeSpanInMinutes);
        /// <summary>
        /// Retrieves the schedule.
        /// </summary>
        /// <param name="stopId">The stop identifier.</param>
        /// <param name="routeId">The route identifier.</param>
        /// <param name="inquiryStartTime">The inquiry start time.</param>
        /// <param name="maxScheduleEntryCount">The maximum schedule entry count.</param>
        /// <returns></returns>
        List<DateTime> RetrieveSchedule(int stopId, int routeId, DateTime inquiryStartTime, int maxScheduleEntryCount);

    }
}

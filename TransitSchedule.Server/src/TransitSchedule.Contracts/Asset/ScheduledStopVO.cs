﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Asset
{
    public class ScheduledStopVO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledStopVO"/> class.
        /// </summary>
        /// <param name="stationId">The station identifier.</param>
        /// <param name="stopTime">The stop time.</param>
        public ScheduledStopVO(int stationId, DateTime stopTime)
        {
            this.StationId = stationId;
            this.ScheduledTime = stopTime;
        }


        /// <summary>
        /// Gets or sets the station identifier.
        /// </summary>
        /// <value>
        /// The station identifier.
        /// </value>
        public int StationId { get; set; }
        /// <summary>
        /// Gets or sets the scheduled time.
        /// </summary>
        /// <value>
        /// The scheduled time.
        /// </value>
        public DateTime ScheduledTime { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.ScheduledTime.ToString("HH:mm");
        }
    }
}

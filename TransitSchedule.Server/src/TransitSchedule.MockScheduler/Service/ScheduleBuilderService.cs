﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using TransitSchedule.Contracts.Service;

namespace TransitSchedule.MockScheduler.Service
{
    public class SimplifiedNonGeoScheduleBuilderService : IScheduleBuilderService
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="SimplifiedNonGeoScheduleBuilderService"/> class.
        /// </summary>
        public SimplifiedNonGeoScheduleBuilderService()
        {

        }
         
        private const int defaultIntervalDelay = 15; //minutes
        private const int defaultTravelDelay = 2; //minutes


        private int lastStopId = 0;
        private DateTime lastStopTime = DateTime.Now.Date;


        /// <summary>
        /// Populates the schedule.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <param name="maxTimeSpanToGenerate">The maximum time span to generate.</param>
        public void PopulateSchedule(RouteVO route, TimeSpan maxTimeSpanToGenerate)
        {
            Stack<DateTime> originSeedTime = new Stack<DateTime>();
            originSeedTime.Push(route.DailyStartTime);

            foreach (var station in route.Path)
            {
                var generatedSpan = new TimeSpan(0);

                while (generatedSpan < maxTimeSpanToGenerate)
                {
                    var interval = new TimeSpan(0);
                    if (lastStopId < 1)
                    {
                        //First Schedule Entry
                        lastStopTime = originSeedTime.Peek();  
                    }
                    else
                    {
                       // interval = this.GetTravelTime(route.Id, lastStopId, station.Id);
                        interval = this.GetTimeToNextDeparture(route.Id, station.Id);
                    }

                    //Set next point
                    lastStopId = station.Id; 
                    lastStopTime = lastStopTime.Add(interval);
                      
                    //Append to Schedule
                    station.AppendScheduledStop(route, lastStopTime);

                    //Update condition to exit loop
                    generatedSpan = generatedSpan.Add(interval);

                }

                //set station/stop reference markers for builder
                originSeedTime.Push(originSeedTime.Peek().Add(GetTravelTime(route.Id, lastStopId, lastStopId+1))  );
                lastStopId = 0;
            } 
        }

        /// <summary>
        /// Gets the travel time.
        /// </summary>
        /// <param name="routeId">The route identifier.</param>
        /// <param name="lastStop">The last stop.</param>
        /// <param name="nextStop">The next stop.</param>
        /// <returns></returns>
        private TimeSpan GetTravelTime(int routeId, int lastStop, int nextStop)
        {
            return new TimeSpan(0, defaultTravelDelay, 0); //def: 2
        }

        /// <summary>
        /// Gets the time to next departure.
        /// </summary>
        /// <param name="routeId">The route identifier.</param>
        /// <param name="station">The station.</param>
        /// <returns></returns>
        private TimeSpan GetTimeToNextDeparture(int routeId, int station)
        {
            return new TimeSpan(0, defaultIntervalDelay, 0); //def: 15
        }
    }
}

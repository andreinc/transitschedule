﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using Xunit;

namespace TransitSchedule.Tests.Contracts.Asset
{
    public class StationVOTests
    {
        [Fact]
        void TestInitStopsWithIncompleteData()
        {
            var stationA = new StationVO(10);

            //Test Init
            Assert.Equal<int>(10, stationA.Id);

            //Expected nothing added
            Assert.Empty(stationA.GetScheduledStops(null));

            //Expected nothing added
            stationA.AppendScheduledStop(null, DateTime.Now);
            Assert.Empty(stationA.GetScheduledStops(null));
             
        }

    }
}

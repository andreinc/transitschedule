﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;

namespace TransitSchedule.Contracts.Service
{
    public interface IScheduleBuilderService
    {
        /// <summary>
        /// Populates the schedule.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <param name="maxTimeSpanToGenerate">The maximum time span to generate.</param>
        void PopulateSchedule(RouteVO route, TimeSpan maxTimeSpanToGenerate);
    }
}

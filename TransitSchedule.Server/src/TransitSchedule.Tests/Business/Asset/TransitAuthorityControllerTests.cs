﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Business.Asset;
using TransitSchedule.Contracts.Business;
using TransitSchedule.CommonUtils.Assets;
using Xunit;

namespace TransitSchedule.Tests.Business.Asset
{
    public class TransitAuthorityControllerTests
    {

        //TODO - with IoC - allow for Test simplified specific Mock object on providers/services


        //Should be more extensive - at this point Primarily relying on the fact that the controller
        //is receiving valid data from the Builder/Providers as they are hard coded
        //the schedule is validated using the 

        [Fact]
        void Validate24HoursSchedule()
        {
            //ref: 
            //      "\TransitSchedule\doc\validation\ManualValidationWorkbook.xlsx"

            //Dump out of schedule into the validation File - checks out
            //Could possible extend this out to spot check
            //or look for edge cases

            Assert.True(true);
        }

        [Fact]
        void TestProblemDefinitionStation1Case()
        {
            ITransitController ctrl = new TransitAuthorityAgent();
            ctrl.Initialize();

            //12:00
            var timeCriteria = DateTime.Now.Date.AddHours(12).AddMinutes(0);

            //Route: 1 - Station 1
            var StationId = 1;
            var limitResult = 4;
            var nextSet1 = ctrl.GetRoute(1).NextOnSchedule(StationId, timeCriteria, limitResult);
            var nextSet2 = ctrl.GetRoute(2).NextOnSchedule(StationId, timeCriteria, limitResult);
            var nextSet3 = ctrl.GetRoute(3).NextOnSchedule(StationId, timeCriteria, limitResult);

            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(00), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(15), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(30), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(45), nextSet1);



            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(02), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(17), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(32), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(47), nextSet2);


            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(04), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(19), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(34), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(49), nextSet3);

        }

        [Fact]
        void TestProblemDefinitionStation2Case()
        {
            ITransitController ctrl = new TransitAuthorityAgent();
            ctrl.Initialize();


            //12:00
            var timeCriteria = DateTime.Now.Date.AddHours(12).AddMinutes(0);

            //Route: 1 - Station 1
            var StationId = 2;
            var limitResult = 4;
            var nextSet1 = ctrl.GetRoute(1).NextOnSchedule(StationId, timeCriteria, limitResult);
            var nextSet2 = ctrl.GetRoute(2).NextOnSchedule(StationId, timeCriteria, limitResult);
            var nextSet3 = ctrl.GetRoute(3).NextOnSchedule(StationId, timeCriteria, limitResult);

            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(02), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(17), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(32), nextSet1);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(47), nextSet1);



            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(04), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(19), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(34), nextSet2);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(49), nextSet2);


            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(06), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(21), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(36), nextSet3);
            Assert.Contains<DateTime>(DateTime.Now.Date.AddHours(12).AddMinutes(51), nextSet3);

        }
    }
}

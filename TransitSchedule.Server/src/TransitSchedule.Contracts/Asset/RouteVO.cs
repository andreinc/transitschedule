﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Asset
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class RouteVO
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteVO"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public RouteVO(int id) : this(id, TimeSpan.Zero )
        {
             
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteVO"/> class.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="startTimeOffset">The start time offset.</param>
        public RouteVO(int Id, TimeSpan startTimeOffset)
        {
            this.Id = Id;
            this.Label = string.Format("Route " + Id);
            this.DailyStartTime = DateTime.Now.Date.Add(startTimeOffset);
            this.Path = new List<StationVO>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public List<StationVO> Path { get; set; }


        /// <summary>
        /// Gets or sets the daily start time.
        /// </summary>
        /// <value>
        /// The daily start time.
        /// </value>
        public DateTime DailyStartTime { get; set; }


        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
} 

//index.html page logic
"use strict";

var scheduleFetchAllowed = 1;
var stopId = 1;	//defaulted to stop 1 as in 
var APIHost = "http://localhost";
var APIPort = ":5000";
var APIUrl =  APIHost + APIPort + "/api/StopSchedule/";

function SetActiveStopId(Id)
{  
	stopId = Id; 
	FetchPresentScheduleForStop();
}

function SetActiveFetch(state)
{
	scheduleFetchAllowed = state;
}

function ClearPrintedSchedule()
{ 
	$("#message").html("");
	$("#schedule").html("");
}

function FetchPresentStationNetwork()
{
	//Mock work from TransitNetwork Provider
	var network = [];
 
		for (var i = 1; i <= 10; i++) {
			//as per assignment spec - only allow station 1 and 2
			network.push({Id:i, Label:"Station " + i, active: i > 2 ? "disabled" : ""})
		} 

		var items = [];
		$.each( network, function( key, val ) {  
			
			 if (key == 0)
			 {
				 val.active = "active";
			 }

			 items.push
			 ( "<li id='stop"+ val.Id +"'  class='"+ val.active +"'><a data-toggle='tab' onclick='SetActiveStopId("+ val.Id +");'>Stop "+ val.Id +"</a></li>" );
		});
 
		//Next Time Entries
		$("#navNetwork").append(  
			  items.join("")  
		  );
 
}

function FetchPresentScheduleForStop()
{
	//Clear printed info
	ClearPrintedSchedule();

	//Check for refresh switch
	if (scheduleFetchAllowed > 0){
  
		//Calls the WebAPI
		$.getJSON( APIUrl + stopId, function( data ) {

		 $.each( data.response, function( key, val ) {
				 
				var routeInfo = ( "<span id='" + val.RouteId + "'>" + val.RouteLabel + " in </span>" );

				var items = [];

				//build array of formatted time entries for UI
				$.each( val.NextArrivalSet, function( key, val ) { 

					var a = moment(data.criteriaDateTime);
					var b = moment(val);
					var c = b.diff(a, 'minutes');   
					var d = b.diff(a, 'seconds') % 60; 
					var e = (b.diff(a, 'seconds') % 60) > 30 ? c + 1 : c; // holds the ceiling minutes from existing seconds

					//items.push( "<span class='scheduledTime' value='" + val + "'>" + c + " mins " + d + " secs " + "</span>" );
					items.push( "<span class='scheduledTime' value='" + val + "'>" + e + " mins "  + "</span>" );
				});

				//Display Content
				//
				//Route Info
				$("#schedule").append(routeInfo);
	
				//Next Time Entries
				$("#schedule").append(  $( "<span/>", { 
					html: items.join( " and " )
				}) );

				$("#schedule").append("<br/>");

				
				$("#message").html( "<i>Last Update: " + moment().format("H:mm:ss") + "</i>");

			});	// end for loop [superflous - original:  the array of time entries]
			
 
			})	// end of JSON call
			.success(function(resp, status) {  	//SUCCESS
				console.info("GET call / 200 / OK"); 
			})									//ERROR
			.error(function(err, status) {  
				console.info("GET call / 500 / ERROR"); 
				console.error(err); 
				SetActiveFetch(0);
				
				$("#btnOn").removeClass("active");
				$("#btnOff").removeClass("active");
				$("#btnOff").addClass("active");
				$("#message").html("<p class='bg-danger'>Sorry. An error has occured in the processing of the request. A error log entry has been recorded.</p>");
			}); //end JSON call
	}
}

function PrintTodayCalendarDate()
{ 
    // Output the day, date, month and year    
    $('#Date').html(moment().format("dddd, MMMM Do YYYY"));
}

function StartTheClock()
{
    setInterval( function() {
	// Create moment() get the seconds
	var seconds = moment().seconds();
	// Add a leading zero to seconds value
	if (scheduleFetchAllowed > 0){
		$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
	}
	},1000);
	
	setInterval( function() {
		// Create moment() get the minutes
		var minutes = moment().minutes();
		// Add a leading zero to the minutes value
		if (scheduleFetchAllowed > 0){
			$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		}
		},1000);
		
	setInterval( function() {
		// Create moment() get the hours
		var hours = new moment().hours();
		// Add a leading zero to the hours value
		if (scheduleFetchAllowed > 0){
			$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
		}
		}, 1000);

			
	setInterval( function() {


		if (scheduleFetchAllowed > 0){
			var seconds = moment().seconds();

			//if (seconds % 15 == 0)	//Update UI every 15 sec
			if (seconds == 0)			//Update UI every minute
			{   
				FetchPresentScheduleForStop(); 
			}//time test end
		}
	
		}, 1000);
} // End Block for Clock/Timer init function


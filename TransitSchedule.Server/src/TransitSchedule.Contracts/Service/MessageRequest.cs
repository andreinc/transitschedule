﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Service
{
    public class MessageRequest<T> where T : new()
    {
        public T criteria;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageRequest{T}"/> class.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        public MessageRequest(T criteria)
        {
            this.userIdentifier = string.Empty;
            this.criteria = criteria;
            this.criteriaDateTime = DateTime.Now;
        }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string userIdentifier { get; set; }
        /// <summary>
        /// Gets or sets the criteria date time.
        /// </summary>
        /// <value>
        /// The criteria date time.
        /// </value>
        public DateTime criteriaDateTime { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using TransitSchedule.Contracts.Service;

namespace TransitSchedule.MockScheduler.Provider
{
    public class HardCodedTransitNetworkProvider : ITransitNetworkProvider
    {
        /// <summary>
        /// Gets the network station set.
        /// </summary>
        /// <returns></returns>
        public IList<StationVO> GetNetworkStationSet()
        {
            var entries = new List<StationVO>();

            for (int i = 1; i <= 10; i++)
            {
                entries.Add(new StationVO(i));
            };

            return entries;

        }
    }
}

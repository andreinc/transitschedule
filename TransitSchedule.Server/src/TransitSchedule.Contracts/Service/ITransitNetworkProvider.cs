﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;

namespace TransitSchedule.Contracts.Service
{
    public interface ITransitNetworkProvider
    {
        IList<StationVO> GetNetworkStationSet();
    }
}

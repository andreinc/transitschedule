﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TransitSchedule.Business.Asset;
using TransitSchedule.Contracts.Asset;
using TransitSchedule.CommonUtils.Assets;
using TransitSchedule.Contracts.Service;
using Microsoft.AspNetCore.Cors;

namespace TransitSchedule.AppServer.Controllers
{
    [Route("api/[controller]")] 
    public class StopScheduleController : Controller
    {
        private int scheduleFetchLimit = 2;
        private TransitAuthorityAgent businessAgent;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="StopScheduleController"/> class.
        /// </summary>
        public StopScheduleController()
        {
            //Initial init of the schedule agent
            GetBusinessAgent();
        }


        /// <summary>
        /// Gets the business agent.
        /// </summary>
        /// <returns></returns>
        private TransitAuthorityAgent GetBusinessAgent()
        {
            //TODO: complete singleton by persisting the agent and its data
            if (businessAgent == null)
            {
                this.businessAgent = new TransitAuthorityAgent();
                //call to generate a schedule for the next 24 hours and 20 minutes
                this.businessAgent.Initialize(24, 10);
            }

            return this.businessAgent;
        }

        #endregion


        #region API


        [EnableCors("AllowAllOrigins")]
        // GET api/values/5
        [HttpGet("{stationId:int}")]
        public MessageResponse<List<RouteScheduleVO>> Get(int stationId)
        { 
            //init the return object
            var returnObj = new MessageResponse<List<RouteScheduleVO>>();

            //set the time criteria used
            returnObj.criteriaDateTime = DateTime.Now; 

            //loop across known routes
            foreach (var route in this.businessAgent.AvailableRoutes)
            {
                //for a given route access its schedule to get next few entries based on time requested
               var scheduleEntries = 
                    this.businessAgent.GetRoute(route.Id)
                                            .NextOnSchedule(stationId, returnObj.criteriaDateTime, scheduleFetchLimit); 

                //add the route details to the response object
                var routeInfo = new RouteScheduleVO(route);
                // sets the scheduled entries
                routeInfo.NextArrivalSet.AddRange(scheduleEntries);
                  
                returnObj.response.Add(routeInfo); 
            }

            //full set is returned
            return returnObj;
        }

        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransitSchedule.Contracts.Asset
{
    public class StationVO
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="StationVO"/> class.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public StationVO(int Id)
        {
            this.Id = Id;
            this.Label = string.Format("Stop " + Id);
            this.Schedule = new Dictionary<RouteVO, List<ScheduledStopVO>>();

        }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string Label { get; set; }



        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        /// <value>
        /// The schedule.
        /// </value>
        private Dictionary<RouteVO, List<ScheduledStopVO>> Schedule { get; set; }

        /// <summary>
        /// Initializes the route.
        /// </summary>
        /// <param name="route">The route.</param>
        private void InitializeRoute(RouteVO route)
        {
            this.Schedule.Add(route, new List<ScheduledStopVO>());
        }

        /// <summary>
        /// Appends the scheduled stop.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <param name="scheduleEntry">The schedule entry.</param>
        public void AppendScheduledStop(RouteVO route, DateTime scheduleEntry)
        {
            if (route != null)
            {
                if (!this.Schedule.ContainsKey(route))
                {
                    InitializeRoute(route);
                }

                this.Schedule[route].Add(new ScheduledStopVO(this.Id, scheduleEntry)); 
            }
        }

        /// <summary>
        /// Gets the scheduled stops.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns></returns>
        public List<ScheduledStopVO> GetScheduledStops(RouteVO route)
        {
            var theList = new List<ScheduledStopVO>();
             
            if (route != null && this.Schedule.ContainsKey(route))
            {
                theList = this.Schedule[route].ToList();
            }

            return theList;

        }
    }
}

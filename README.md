# Platforms
IDE used: Visual Studio 2015 (ASPNET CORE 1.0.0-rc2-3002702) and Visual Studio Code

# Running the Server Application
In a prompt window locate the project's appserver root folder [.\TransitSchedule.Server\src\TransitSchedule.AppServer\]. 
From the AppServer project root folder run the command below to launch the WebAPI application.
web server.
```
dotnet run
```

Alternatively loading the solution [TransitSchedule.sln] into Visual Studio allows the developer to launch the application using either the "IIS Express" or "TransitSchedule.AppServer"(Kestrel) Profiles.


# Running the Client Application
The front end is plain Javascript with jQuery/ajax it can be run from the browser by opening the [.\TransitSchedule.Client\index.html] file.

```
Copy the full path to the index.html file
Paste 
Ensure double quotes on the copied path are removed
Submit the entry in your browser's URL bar
```

# API url on the client page
Connectivity details on the client side referring to the the application server hostname as well as the port are hardcoded in the [./TransitSchedule.Client/js/index.js] file.
```
Expected API url: http://localhost:40063/api/StopSchedule/{id:int}
```

# Client Frameworks
* jQuery/ajax
* Moment.js
* Bootstrap 3

# Mock Data validation
System-generated data was validated using this MSExcel file. The first sheet is formatted to highlight any data issues against the expected set of data.
[./TransitSchedule/TransitSchedule.Server/doc/validation/ManualValidationWorkbook.xlsx]
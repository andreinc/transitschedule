﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;

namespace TransitSchedule.CommonUtils.Assets
{
    public static class TransitAuthorityAgentUtils
    {



        /// <summary>
        /// Prints the available schedule.
        /// </summary>
        /// <param name="routeId">The route identifier.</param>
        /// <returns></returns>
        public static string PrintAvailableSchedule(this IList<RouteVO> availableRoutes, int routeId)
        {
            var sb = new StringBuilder();

            var route = availableRoutes.Where(r => r.Id == routeId).SingleOrDefault();

            sb.Append(route.Label);
            sb.AppendLine();
            foreach (var station in route.Path)
            {
                sb.AppendFormat("{0};", station.Label);

                var stops = station.GetScheduledStops(route).Where(ss => ss.StationId == station.Id).ToList();
                var steps = String.Join(";", stops);
                sb.Append(steps);

                sb.AppendLine();
            }

            return sb.ToString();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Business.Asset;
using TransitSchedule.CommonUtils.Assets;

namespace TransitSchedule.ConsoleApp
{
    /// <summary>
    /// Used for smoke testing implemented set of features - no assertions
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            var ctrl = new TransitAuthorityAgent();
            ctrl.Initialize(24, 4);

            PrintGeneratedSchedule(ctrl);

            FetchSomeGenericSchedulePerStation(ctrl);

            SimulateOutputFromProblemText(ctrl);

        }


        /// <summary>
        /// Fetches some generic schedule per station.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        private static void FetchSomeGenericSchedulePerStation(TransitAuthorityAgent ctrl)
        {
            var dt = DateTime.Now.Date;
            var aTime = dt.AddHours(0).AddMinutes(14);

            var a = ctrl.GetRoute(1).NextOnSchedule(2, aTime, 2);
            var b = ctrl.GetRoute(2).NextOnSchedule(7, aTime.AddHours(3), 4);
            var c = ctrl.GetRoute(3).NextOnSchedule(10, aTime, 4);
        }

        /// <summary>
        /// Prints the generated schedule.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        private static void PrintGeneratedSchedule(TransitAuthorityAgent ctrl)
        {
            var scheduleR1 = ctrl.AvailableRoutes.PrintAvailableSchedule(1);
            Console.Write(scheduleR1);
            var scheduleR2 = ctrl.AvailableRoutes.PrintAvailableSchedule(2);
            Console.Write(scheduleR2);
            var scheduleR3 = ctrl.AvailableRoutes.PrintAvailableSchedule(3);
            Console.Write(scheduleR3);
        }



        #region SimulateOutputFromProblemText
        /// <summary>
        /// Simulates the output from problem text.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        private static void SimulateOutputFromProblemText(TransitAuthorityAgent ctrl)
        {

            //TODO: As (mins) from Now to be proper


            var stationIdSet = new int[] { 1, 2 };
            var timeCriteria = DateTime.Now;
            Console.WriteLine(string.Format("Requested Time {0}: ", timeCriteria));
            foreach (var stationId in stationIdSet)
            {
                Console.WriteLine(string.Format("Stop {0} schedule: ", stationId));
                foreach (var route in ctrl.AvailableRoutes)
                {
                    Console.Write(string.Format("{0}: ", route.Label));
                    ConsolePrintSchedule(ctrl, route, stationId, timeCriteria);
                }
            }
        }

        /// <summary>
        /// Consoles the print schedule.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        /// <param name="route">The route.</param>
        /// <param name="stationId">The station identifier.</param>
        /// <param name="timeCriteria">The time criteria.</param>
        private static void ConsolePrintSchedule(TransitAuthorityAgent ctrl, Contracts.Asset.RouteVO route, int stationId, DateTime timeCriteria)
        {
            var nextSet = ctrl.GetRoute(route.Id).NextOnSchedule(stationId, timeCriteria, 2);
            Console.Write(String.Join(", ", nextSet.Select(d=>d.ToString("H:mm"))));
            Console.Write("...");
            Console.WriteLine(string.Empty);
        }

        #endregion
    }
}

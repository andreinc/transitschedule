﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;
using Xunit;

namespace TransitSchedule.Tests.Contracts.Asset
{
    public class RouteVOTests
    {
        [Fact]
        void TestInitPathAndAddSingleStation()
        {
            var routeA = new RouteVO(10);

            Assert.Empty(routeA.Path);

            routeA.Path.Add(new StationVO(100));
            Assert.Equal<int>(routeA.Path.FirstOrDefault().Id, 100);

            Assert.Equal<int>(routeA.Path.Count, 1); 
        }

     
    }
}

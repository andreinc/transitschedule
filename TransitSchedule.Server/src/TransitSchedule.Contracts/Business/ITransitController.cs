﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransitSchedule.Contracts.Asset;

namespace TransitSchedule.Contracts.Business
{
    public interface ITransitController
    {
        void Initialize();
        RouteVO GetRoute(int routeId);
    }
}
